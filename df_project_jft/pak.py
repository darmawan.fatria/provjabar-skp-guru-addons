from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
import time
from mx import DateTime
from openerp import netsvc

_MIN_UNSUR_UTAMA = 2
class penentuan_angka_kredit(osv.Model):
    _name = "penentuan.angka.kredit"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = "Penetapan Angka Kredit"

    def create(self, cr, uid, vals, context=None):
        if vals:
            for user_obj in self.pool.get('res.users').browse(cr, uid, uid, context=context) :
                employee = user_obj.partner_id
                user_id_atasan = employee.user_id_atasan.user_id.id
                user_id_banding = employee.user_id_banding.user_id.id
                user_id_bkd = employee.company_id.user_id_bkd.id
                user_id_pak = employee.company_id.user_id_pak.id

                vals['user_id_atasan'] = user_id_atasan
                vals['user_id_banding'] =  user_id_banding
                vals['user_id_bkd'] = user_id_bkd
                vals['user_id_pak'] = user_id_pak

        return super(penentuan_angka_kredit, self).create(cr, uid, vals, context)

    def write(self, cr, uid, ids, vals, context=None):
        if not isinstance(ids, list): ids = [ids]
        for task_obj in self.browse(cr, uid, ids, context=context):
            task_id = task_obj.id
            if not task_obj.state:
                if uid != SUPERUSER_ID:
                    if task_obj.state in ('draft', 'realisasi', 'rejected_manager'):
                        if not self.is_user(cr, uid, ids, context):
                            return False
                    if task_obj.state in ('propose', 'rejected_bkd'):
                        if not self.is_manager(cr, uid, ids, context):
                            return False
                    if task_obj.state in ('appeal'):
                        if not self.is_appeal_manager(cr, uid, ids, context):
                            return False
                    if task_obj.state in ('evaluated'):
                        if not self.is_verificator(cr, uid, ids, context):
                            return False
                    if task_obj.state in ('done', 'cancelled'):
                        if not self.is_verificator(cr, uid, ids, context):
                            return False
                    if task_obj.state in ('pak_evaluated'):
                        if not self.is_pak_verificator(cr, uid, ids, context):
                            return False
            elif task_obj.state == 'confirm':
                if uid != SUPERUSER_ID:
                    if task_obj.state in ('draft', 'realisasi', 'rejected_manager', 'cancelled'):
                        if not self.is_user(cr, uid, ids, context):
                            return False
                    if task_obj.state in ('propose', 'rejected_bkd'):
                        if not self.is_manager(cr, uid, ids, context):
                            return False
                    if task_obj.state in ('appeal'):
                        if not self.is_appeal_manager(cr, uid, ids, context):
                            return False
                    if task_obj.state in ('evaluated'):
                        if not self.is_verificator(cr, uid, ids, context):
                            return False
                    if task_obj.state in ('done'):
                        if not self.is_verificator(cr, uid, ids, context):
                            return False
                    if task_obj.state in ('pak_evaluated'):
                        if not self.is_pak_verificator(cr, uid, ids, context):
                            return False
            elif task_obj.state == 'closed':
                if uid != SUPERUSER_ID:
                    if task_obj.state in ('draft', 'realisasi', 'rejected_manager', 'cancelled'):
                        if not self.is_user(cr, uid, ids, context):
                            return False
                    if task_obj.state in ('propose', 'rejected_bkd'):
                        if not self.is_manager(cr, uid, ids, context):
                            return False
                    if task_obj.state in ('appeal'):
                        if not self.is_appeal_manager(cr, uid, ids, context):
                            return False
                    if task_obj.state in ('evaluated'):
                        if not self.is_verificator(cr, uid, ids, context):
                            return False
                    if task_obj.state in ('done', 'closed'):
                        if not self.is_verificator(cr, uid, ids, context):
                            return False
                    if task_obj.state in ('pak_evaluated'):
                        if not self.is_pak_verificator(cr, uid, ids, context):
                            return False
        super(penentuan_angka_kredit, self).write(cr, uid, ids, vals, context=context)
        return True

    def onchange_field_selisih_ak(self, cr, uid, ids, angka_kredit_jabatan_atas, angka_kredit_terakhir,  context=None):
        vals = {}
        target_angka_kredit = 0
        if angka_kredit_jabatan_atas  :
            if angka_kredit_jabatan_atas > 0:
                target_angka_kredit = angka_kredit_jabatan_atas - angka_kredit_terakhir;
                vals.update(
                    {'target_angka_kredit': target_angka_kredit,
                     'fn_selisih_angka_kredit': target_angka_kredit})

        return {'value': vals}

    def onchange_field_minimum_pencapaian_ak(self, cr, uid, ids, target_angka_kredit, lama_kegiatan,target_period_year, context=None):
        vals = {}
        #minimum ak
        minimum_pencapaian_angka_kredit = 0
        if target_angka_kredit and lama_kegiatan:
            if target_angka_kredit > 0 and lama_kegiatan > 0:
                minimum_pencapaian_angka_kredit = float(target_angka_kredit / int(lama_kegiatan))
                vals.update(
                    {'minimum_pencapaian_angka_kredit': minimum_pencapaian_angka_kredit,
                     'fn_minimum_pencapaian_angka_kredit': minimum_pencapaian_angka_kredit})

        #year
        target_period_year_akhir = 1990
        if lama_kegiatan and target_period_year:
            if lama_kegiatan > 0:
                target_period_year_akhir = (int(target_period_year) + int(lama_kegiatan) ) -1 ;
                period = 'Periode ' + str(target_period_year) + ' - ' + str(target_period_year_akhir)
                vals.update(
                    {'target_period_year_akhir': str(target_period_year_akhir),
                     'name': period
                     })

        return {'value': vals}

    def onchange_field_target_period_year(self, cr, uid, ids, lama_kegiatan, target_period_year, context=None):
        vals = {}
        target_period_year_akhir = 1990
        if lama_kegiatan and target_period_year:
            if lama_kegiatan > 0:
                target_period_year_akhir = (int(target_period_year) + int(lama_kegiatan)) -1;
                period = 'Periode '+ str(target_period_year) +' - '+str(target_period_year_akhir)
                vals.update(
                    {'target_period_year_akhir': str(target_period_year_akhir),
                     'name' : period
                     })
        return {'value': vals}

    def _get_total_pencapaian_ak(self, cr, uid, ids, name, arg, context=None):
        res = {}
        if not ids:
            return res
        for _target in self.browse(cr, uid, ids, context=context):
            res[_target.id] = 0.0
            if _target.pak_detail_ids:
                for _detail in _target.pak_detail_ids:
                    res[_target.id] += _detail.target_angka_kredit

        return res

    def _get_total_pencapaian_ak_utama(self, cr, uid, ids, name, arg, context=None):
        res = {}
        if not ids:
            return res
        for _target in self.browse(cr, uid, ids, context=context):
            res[_target.id] = 0.0
            if _target.pak_detail_ids:
                for _detail in _target.pak_detail_ids:
                    res[_target.id] += _detail.target_angka_kredit_utama

        return res

    def _get_total_pencapaian_ak_penunjang(self, cr, uid, ids, name, arg, context=None):
        res = {}
        if not ids:
            return res
        for _target in self.browse(cr, uid, ids, context=context):
            res[_target.id] = 0.0
            if _target.pak_detail_ids:
                for _detail in _target.pak_detail_ids:
                    res[_target.id] += _detail.target_angka_kredit_penunjang

        return res

    def _get_percent_pencapaian_ak_utama(self, cr, uid, ids, name, arg, context=None):
        res = {}
        if not ids:
            return res
        for _target in self.browse(cr, uid, ids, context=context):
            res[_target.id] = 0.0
            if _target.pak_detail_ids:
                res[_target.id] = float((_target.total_pencapaian_angka_kredit_utama / _target.total_pencapaian_angka_kredit)) * 100.00

        return res

    def _get_percent_pencapaian_ak_penunjang(self, cr, uid, ids, name, arg, context=None):
        res = {}
        if not ids:
            return res
        for _target in self.browse(cr, uid, ids, context=context):
            res[_target.id] = 0.0
            if _target.pak_detail_ids:
                res[_target.id] = float((_target.total_pencapaian_angka_kredit_penunjang / _target.total_pencapaian_angka_kredit)) * 100.00
        return res
    def _get_selisih_ak(self, cr, uid, ids, name, arg, context=None):
        res = {}
        if not ids:
            return res
        for _target in self.browse(cr, uid, ids, context=context):
            res[_target.id] = 0.0
            if _target.angka_kredit_jabatan_atas > 0:
                res[_target.id] = _target.angka_kredit_jabatan_atas - _target.angka_kredit_terakhir
        return res

    def _get_minimum_pencapaian_ak(self, cr, uid, ids, name, arg, context=None):
        res = {}
        if not ids:
            return res
        for _target in self.browse(cr, uid, ids, context=context):
            res[_target.id] = 0.0
            selisih_ak = _target.fn_selisih_angka_kredit
            if selisih_ak > 0 and _target.lama_kegiatan:
                res[_target.id] = float(selisih_ak / int(_target.lama_kegiatan))
        return res
    _columns = {
        'name': fields.char('Periode DUPAK', size=500, required=True, readonly=True,
                            states={'draft': [('readonly', False)], 'new': [('readonly', False)]}),
        'state': fields.selection([('draft', 'Draft'), ('new', 'Baru'),
                                   ('propose', 'Penilaian Atasan'), ('rejected_manager', 'Penilaian Ditolak'),
                                   ('pak_evaluated', 'Verifikasi Tim PAK'),
                                   ('evaluated', 'Verifikasi BKD'), ('rejected_bkd', 'Pengajuan Ditolak BKD'),
                                   ('confirm', 'Target Di Terima'),
                                   ('pending', 'Pending'),
                                   ('propose_to_close', 'Pengajuan Closing Target'), ('closed', 'Closed'),
                                   ('cancelled', 'Cancel'),
                                   ('propose_correction', 'Ajukan Perubahan Target'),
                                   ('correction', 'Revisi Target'),
                                   ], 'Status', required=True, ),
        'lama_kegiatan': fields.selection([ ('1', '1 Tahun'),
                                           ('2', '2 Tahun'),
                                           ('3', '3 Tahun'),
                                           ('4', '4 Tahun'),
                                           ],
                                          'Lama Kegiatan', required=True, readonly=True,
                                            states={'draft': [('readonly', False)], 'new': [('readonly', False)]}),
        'target_period_year': fields.char('Periode Awal Target', size=4, required=True, readonly=True,
                            states={'draft': [('readonly', False)], 'new': [('readonly', False)]}),
        'target_period_year_akhir': fields.char('Periode Akhir Target', size=4, readonly=True, ),
        'notes': fields.text('Catatan'),
        'angka_kredit_terakhir': fields.float('AK Terakhir', readonly=True, required=True,
                                              states={'draft': [('readonly', False)], 'new': [('readonly', False)]}
                                              , digits_compute=dp.get_precision('angka_kredit')),
        'angka_kredit_jabatan_atas': fields.float('AK Jabatan Diatas', readonly=True, required=True,
                                                  states={'draft': [('readonly', False)], 'new': [('readonly', False)]}
                                                  , digits_compute=dp.get_precision('angka_kredit')),
        'target_angka_kredit': fields.float('Target AK',
                                                  digits_compute=dp.get_precision('angka_kredit')),
        'minimum_pencapaian_angka_kredit': fields.float('Minimum Pencapaian AK',
                                            digits_compute=dp.get_precision('angka_kredit')),

        'total_pencapaian_angka_kredit': fields.function(_get_total_pencapaian_ak, method=True, readonly=True,
                                                           string='Total', store=False, digits_compute=dp.get_precision('angka_kredit')),
        'total_pencapaian_angka_kredit_utama': fields.function(_get_total_pencapaian_ak_utama, method=True, readonly=True,
                                                         string='Unsur Utama', store=False, digits_compute=dp.get_precision('angka_kredit')),
        'total_pencapaian_angka_kredit_penunjang': fields.function(_get_total_pencapaian_ak_penunjang, method=True, readonly=True,
                                                         string='Unsur Penunjang', store=False, digits_compute=dp.get_precision('angka_kredit')),
        'fn_selisih_angka_kredit': fields.function(_get_selisih_ak, method=True, readonly=True,
                                                string='Selisih AK', store=False),
        'fn_minimum_pencapaian_angka_kredit': fields.function(_get_minimum_pencapaian_ak, method=True, readonly=True,
                                                           string='Minimum Pencapaian AK', store=False,
                                                           digits_compute=dp.get_precision('angka_kredit')),
        'percent_pencapaian_angka_kredit_utama': fields.function(_get_percent_pencapaian_ak_utama, method=True,
                                                               readonly=True,
                                                               string='Unsur Utama', store=False,
                                                               digits_compute=dp.get_precision('no_digit')),
        'percent_pencapaian_angka_kredit_penunjang': fields.function(_get_percent_pencapaian_ak_penunjang, method=True,
                                                                   readonly=True,
                                                                   string='Unsur Penunjang', store=False,
                                                                   digits_compute=dp.get_precision('no_digit')),

        'pak_detail_ids': fields.one2many('project.pak', 'pak_id', 'Rincian Penetapan Angka Kredit',
                                           readonly=True,states={'draft': [('readonly', False)], 'new': [('readonly', False)]},
                                           order='target_period_year asc'),
        'skp_tahunan_ids': fields.one2many('project.project', 'pak_id', 'SKP Tahunan',
                                          readonly=True,
                                          states={'draft': [('readonly', False)], 'new': [('readonly', False)]},
                                          order='target_period_year asc'),
        'user_id': fields.many2one('res.users', 'Pegawai Yang Dinilai', ),
        'user_id_atasan': fields.many2one('res.users', 'Atasan Langsung', ),
        'user_id_banding': fields.many2one('res.users', 'Atasan Banding', ),
        'user_id_bkd': fields.many2one('res.users', 'Verifikatur', ),
        'user_id_pak': fields.many2one('res.users', 'Verifikatur PAK', ),

        'active': fields.boolean('Active'),
        'employee_id': fields.related('user_id', 'partner_id', relation='res.partner', type='many2one',
                                      string='Data Pegawai', store=True),
        'is_guru': fields.related('employee_id', 'is_guru', type='boolean', string='Guru',
                                        store=True),
        'company_id': fields.many2one('res.company', 'OPD'),

    }
    _defaults = {
        'active': True,
        'target_period_year': lambda *args: time.strftime('%Y'),
        'user_id': lambda self, cr, uid, ctx: uid,
        'company_id': lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,
        'state': 'draft',
    }
    _order = "write_date desc"



    # VALIDATOR
    def get_auth_id(self, cr, uid, ids, type, context=None):
        if not isinstance(ids, list): ids = [ids]
        if uid == SUPERUSER_ID:
            return True
        for task in self.browse(cr, uid, ids, context=context):

            if type == 'user_id':
                if task.user_id:
                    if task.user_id.id != uid:
                        raise osv.except_osv(_('Invalid Action!'),
                                             _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
            elif type == 'user_id_atasan':
                if task.user_id_atasan:
                    if task.user_id_atasan.id != uid:
                        raise osv.except_osv(_('Invalid Action!'),
                                             _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
            elif type == 'user_id_banding':
                if task.user_id_banding:
                    if task.user_id_banding.id != uid:
                        raise osv.except_osv(_('Invalid Action!'),
                                             _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
            elif type == 'user_id_bkd':
                if task.user_id_bkd:
                    if task.user_id_bkd.id != uid:
                        raise osv.except_osv(_('Invalid Action!'),
                                             _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
            elif type == 'user_id_pak':
                if task.user_id_pak:
                    if task.user_id_pak.id != uid:
                        raise osv.except_osv(_('Invalid Action!'),
                                             _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))

            else:
                return False;

        return True;

    def is_user(self, cr, uid, ids, context=None):
        task_id = len(ids) and ids[0] or False
        ret_val = False
        if not task_id: return False
        return self.get_auth_id(cr, uid, [task_id], 'user_id', context=context)

    def is_manager(self, cr, uid, ids, context=None):
        task_id = len(ids) and ids[0] or False
        ret_val = False
        if not task_id: return False
        return self.get_auth_id(cr, uid, [task_id], 'user_id_atasan', context=context)

    def is_appeal_manager(self, cr, uid, ids, context=None):
        task_id = len(ids) and ids[0] or False
        ret_val = False
        if not task_id: return False
        return self.get_auth_id(cr, uid, [task_id], 'user_id_banding', context=context)

    def is_verificator(self, cr, uid, ids, context=None):
        task_id = len(ids) and ids[0] or False
        ret_val = False
        if not task_id: return False
        if uid == SUPERUSER_ID: return True;
        return self.get_auth_id(cr, uid, [task_id], 'user_id_bkd', context=context)

    def is_pak_verificator(self, cr, uid, ids, context=None):
        task_id = len(ids) and ids[0] or False
        ret_val = False
        if not task_id: return False
        if uid == SUPERUSER_ID: return True;
        return self.get_auth_id(cr, uid, [task_id], 'user_id_pak', context=context)

    def is_user_or_is_verificator(self, cr, uid, ids, context=None):
        task_id = len(ids) and ids[0] or False
        if not task_id: return False
        accepted = self.get_auth_id(cr, uid, [task_id], 'user_id', context=context) or self.get_auth_id(cr, uid,
                                                                                                        [task_id],
                                                                                                        'user_id_bkd',
                                                                                                        context=context)
        return accepted


    #flow
    def action_propose(self, cr, uid, ids, context=None):
        if self.is_user(cr, uid, ids, context):

            for target in self.browse(cr, uid, ids, context=context):
                total_ak_tahunan = 0
                if self.validate_target(target):
                    if target.pak_detail_ids:
                        for rincian_pak in target.pak_detail_ids:
                            if rincian_pak.jumlah_kegiatan_utama < _MIN_UNSUR_UTAMA:
                                raise osv.except_osv(_('Invalid Action!'),
                                                     _('Jumlah kegiatan per tahun unsur utama kurang dari batas minimum'))
                    if target.skp_tahunan_ids :
                        for skp_tahunan in target.skp_tahunan_ids:
                            total_ak_tahunan = total_ak_tahunan + skp_tahunan.target_angka_kredit

                    if total_ak_tahunan < target.target_angka_kredit :
                        raise osv.except_osv(_('Invalid Action!'),
                                             _('Total angka kredit pada rincian kegiatan tahunan kurang dari target angka kredit '))

                    return self.write(cr, uid, ids, {'state': 'propose'}, context=context)

        return False

    def action_cancel_target(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        if self.is_user_or_is_verificator(cr, uid, ids, context):
            task_pool = self.pool.get('project.pak')
            target_tahunan_pool = self.pool.get('project.project')
            wf_service = netsvc.LocalService("workflow")
            for target_id in ids:
                task_ids = task_pool.search(cr, uid, [('pak_id', '=', target_id)], context=None)
                target_tahunan_ids = target_tahunan_pool.search(cr, uid, [('pak_id', '=', target_id)], context=None)
                if task_ids:
                    for task_id in task_ids:
                        wf_service.trg_validate(SUPERUSER_ID, 'project.pak', task_id, 'act_cancel', cr)
                        task_pool.write(cr, SUPERUSER_ID, task_id, {'active': False}, context=context)
                if target_tahunan_ids:
                    for target_tahunan_id in target_tahunan_ids:
                        wf_service.trg_validate(SUPERUSER_ID, 'project.project', target_tahunan_id, 'act_cancel', cr)
                        target_tahunan_pool.cancel_target(cr, SUPERUSER_ID, [target_tahunan_id],context=context)
                target_tahunan_pool.write(cr, SUPERUSER_ID, target_tahunan_ids, {'active': False}, context=context)
                self.write(cr, SUPERUSER_ID, [target_id], {'state': 'cancelled','active': False}, context=context)
            return True;
        return False;

    def action_set_to_draft(self, cr, uid, ids, context=None):
        if self.is_user(cr, uid, ids, context):
            task_pool = self.pool.get('project.pak')
            target_tahunan_pool = self.pool.get('project.project')
            skp_employee_pool = self.pool.get('skp.employee')
            perilaku_pool = self.pool.get('project.perilaku')
            tugas_tambahan_pool = self.pool.get('project.tambahan.kreatifitas')
            wf_service = netsvc.LocalService("workflow")
            current_year = time.strftime('%Y')
            for target_id in ids:
                task_ids = task_pool.search(cr, uid, [('pak_id', '=', target_id)], context=None)
                target_tahunan_ids = target_tahunan_pool.search(cr, uid, [('pak_id', '=', target_id)], context=None)
                skp_employee_ids = skp_employee_pool.search(cr, uid, [('user_id', '=', uid),('target_period_year', '=', current_year)], context=None)
                perilaku_ids = perilaku_pool.search(cr, uid, [('user_id', '=', uid),('state', '=', 'done'),
                                                                ('target_period_year', '=', current_year)],
                                                            context=None)
                tambahan_ids = tugas_tambahan_pool.search(cr, uid, [('user_id', '=', uid),('state', '=', 'done'),
                                                                      ('target_period_year', '=', current_year)],
                                                            context=None)
                if skp_employee_ids:
                    skp_employee_pool.unlink(cr,SUPERUSER_ID,skp_employee_ids, context=None)
                if task_ids:
                    for task_id in task_ids:
                        wf_service.trg_validate(SUPERUSER_ID, 'project.pak', task_id, 'act_cancel', cr)
                        task_pool.write(cr, SUPERUSER_ID, task_id, {'active': False,'state':'cancelled'}, context=context)
                if target_tahunan_ids:
                    for target_tahunan_id in target_tahunan_ids:
                        wf_service.trg_validate(SUPERUSER_ID, 'project.project', target_tahunan_id, 'act_cancel', cr)
                        target_tahunan_pool.cancel_target(cr, SUPERUSER_ID, [target_tahunan_id], context=context)
                if perilaku_ids:
                    perilaku_pool.write(cr, SUPERUSER_ID, perilaku_ids, {'state':'evaluated'}, context=None)
                if tambahan_ids:
                    tugas_tambahan_pool.write(cr, SUPERUSER_ID, tambahan_ids, {'state':'evaluated'}, context=None)

                target_tahunan_pool.write(cr, SUPERUSER_ID, target_tahunan_ids, {'active': False,'state':'cancelled'}, context=context)
            return self.write(cr, uid,ids , {'state': 'draft'}, context=context)

    def action_pak_evaluated(self, cr, uid, ids, context=None):
        if self.is_manager(cr, uid, ids, context):
            return self.write(cr, uid, ids, {'state': 'pak_evaluated'}, context=context)
    def action_evaluated(self, cr, uid, ids, context=None):
        if self.is_pak_verificator(cr, uid, ids, context):
            return self.write(cr, uid, ids, {'state': 'evaluated'}, context=context)
    def action_propose_rejected(self, cr, uid, ids, context=None):
        if self.is_manager(cr, uid, ids, context):
            return self.write(cr, uid, ids, {'state': 'draft'}, context=context)
    def action_pak_evaluate_rejected(self, cr, uid, ids, context=None):
        if self.is_pak_verificator(cr, uid, ids, context):
            return self.write(cr, uid, ids, {'state': 'draft'}, context=context)

    def action_confirm(self, cr, uid, ids, context=None):
        if self.is_verificator(cr, uid, ids, context):
            if context is None:
                context = {}
            task_pool = self.pool.get('project.pak')

            for target_id in ids:
                task_ids = task_pool.search(cr, uid, [('pak_id', '=', target_id)], context=None)
                if task_ids:
                    task_pool.set_confirm(cr, uid, task_ids, context=context)
                self.write(cr, uid, ids, {'state': 'confirm'}, context=context)
        return True

    def action_evaluate_rejected(self, cr, uid, ids, context=None):
        if self.is_verificator(cr, uid, ids, context):
            return self.write(cr, uid, ids, {'state': 'draft'}, context=context)


    #
    def generate_detail_dupak(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        task = {}
        task_pool = self.pool.get('project.pak')
        for target_obj in self.browse(cr, uid, ids, context=context):
            task_ids = task_pool.search(cr, uid, [('pak_id', '=', target_obj.id)], context=None)
            if task_ids:
                for task_id in task_ids:
                    task_pool.write(cr, SUPERUSER_ID, task_id, {'active': False}, context=context)

            lama_kegiatan = int(target_obj.lama_kegiatan)
            user_id = target_obj.user_id.id
            user_id_bkd = None
            user_id_pak = None
            employee = target_obj.user_id.partner_id
            if user_id != uid:
                raise osv.except_osv(_('Invalid Action!'),
                                     _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
            if not employee:
                raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                     _(
                                         'Proses Tidak Dapat Dilanjutkan Karena Ada Beberapa Informasi Kepegawaian Belum Diisi, Khususnya Data Pejabat Penilai Dan Atasan Banding.'))
            else:
                company = employee.company_id
                company_id = company.id
                if not company_id:
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                         _('Proses Tidak Dapat Dilanjutkan Karena OPD/Dinas Pegawai Belum Dilengkapi.'))

                if not target_obj.user_id_bkd:
                    if not company.user_id_bkd:
                        raise osv.except_osv(_('Invalid Action, Data Dinas Kurang Lengkap'),
                                             _(
                                                 'Verifikatur Dari BKD Belum Diupdate,  Silahkan hubungi Admin Atau isi Data Pemeriksa.'))
                    else:
                        user_id_bkd = company.user_id_bkd.id
                else:
                    user_id_bkd = target_obj.user_id_bkd.id
                if not target_obj.user_id_pak:
                    if not company.user_id_pak:
                        raise osv.except_osv(_('Invalid Action, Data Dinas Kurang Lengkap'),
                                             _(
                                                 'Verifikatur PAK Belum Diupdate,  Silahkan hubungi Admin Atau isi Data Pemeriksa.'))
                    else:
                        user_id_pak = company.user_id_pak.id
                else:
                    user_id_pak = target_obj.user_id_pak.id
                if not employee.user_id_atasan:
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                         _(
                                             'Proses Tidak Dapat Dilanjutkan Karena Data Atasan Langsung (Pejabat Penilai) Belum Terisi.'))
                if not employee.user_id_atasan.user_id:
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                         _(
                                             'Proses Tidak Dapat Dilanjutkan Karena Data Atasan Langsung (Pejabat Penilai) Belum Melakukan Proses Aktifasi.'))
                if not employee.user_id_banding:
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                         _(
                                             'Proses Tidak Dapat Dilanjutkan Karena Data Atasan Banding (Pejabat Pengajuan Banding) Belum Terisi.'))

                if not employee.user_id_banding.user_id:
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                         _(
                                             'Proses Tidak Dapat Dilanjutkan Karena Data Atasan Banding (Pejabat Pengajuan Banding) Belum Melakukan Proses Aktifasi.'))

                if not employee.job_type or not employee.job_id or not employee.golongan_id:
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                         _(
                                             'Proses Tidak Dapat Dilanjutkan Karena Data Jabatan Dan Pangkat Belum Diisi.'))

                if target_obj.target_angka_kredit < 0 :
                    raise osv.except_osv(_('Invalid Action!'),
                                         _('Minimum pencapaian tidak boleh kurang dari 0'))


            user_id_atasan = target_obj.user_id_atasan.id
            user_id_banding = target_obj.user_id_banding.id

            if not target_obj.user_id_atasan.id:
                user_id_atasan = employee.user_id_atasan.user_id.id
            if not target_obj.user_id_banding.id:
                user_id_banding = employee.user_id_banding.user_id.id

            task.update({
                'pak_id': target_obj.id,
                'user_id': user_id,
                'company_id': company_id,
                'user_id_atasan': user_id_atasan or False,
                'user_id_banding': user_id_banding or False,
                'user_id_bkd': user_id_bkd or False,
                'user_id_pak' : user_id_pak or False,
                'active': True,
            })
            # Update Task Target Tahunan
            now = DateTime.today();
            part_angka_kredit = 0
            sum_of_angka_kredit = 0
            ang_kredit = target_obj.target_angka_kredit
            x_kredit = ang_kredit / lama_kegiatan
            y_kredit = ang_kredit % lama_kegiatan

            for i in range(0, lama_kegiatan):
                # angka Kredit
                if ang_kredit > 0:
                    part_angka_kredit = round(x_kredit)
                    sum_of_angka_kredit += part_angka_kredit
                increment_year = int(target_obj.target_period_year) + i
                task.update({
                    'target_period_year': str(increment_year),
                    'name': '-',
                    'target_angka_kredit': part_angka_kredit,
                    'target_angka_kredit_utama': part_angka_kredit,
                    'target_angka_kredit_penunjang': 0,

                })

                if i == (lama_kegiatan - 1):
                    balancing_kredit = ang_kredit - sum_of_angka_kredit
                    task.update({
                        'target_angka_kredit': balancing_kredit + part_angka_kredit,
                        'target_angka_kredit_utama': balancing_kredit + part_angka_kredit,
                    })
                # insert task
                task_id = task_pool.create(cr, uid, task, context)

            # Update Status Target Generate Dupak
            update_target = {
                'generate_dupak':True,
            }
            return self.write(cr, uid, target_obj.id, update_target, context)

        return False

    def generate_skp_tahunan(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        ret_val = False;
        project_project_pool = self.pool.get('project.project')
        project_pak_pool = self.pool.get('project.pak')
        for target in self.browse(cr, uid, ids, context=context):
            skp_tahunan = {}
            if self.validate_target(target) :
                if target.pak_detail_ids :
                    for rincian_pak in target.pak_detail_ids:
                        if rincian_pak.jumlah_kegiatan_utama < _MIN_UNSUR_UTAMA:
                            raise osv.except_osv(_('Invalid Action!'),
                                                 _('Jumlah kegiatan per tahun unsur utama kurang dari batas minimum'))
                        ret_val = project_pak_pool.generate_target_tahunan(cr,uid,rincian_pak.id,context=None)

        return ret_val

    def validate_target(self,target):
        if target.total_pencapaian_angka_kredit < target.minimum_pencapaian_angka_kredit:
            raise osv.except_osv(_('Invalid Action!'),
                                 _('Target pencapaian tidak memenuhi batas minimum'))
        if target.total_pencapaian_angka_kredit <= 0:
            raise osv.except_osv(_('Invalid Action!'),
                                 _('Target pencapaian harus lebih dari 0'))
        if target.target_angka_kredit <= 0:
            raise osv.except_osv(_('Invalid Action!'),
                                 _('Angka kredit terakhir dan Angka kredit jabatan diatas tidak valid'))
        utama_penunjang = target.total_pencapaian_angka_kredit_penunjang + target.total_pencapaian_angka_kredit_utama
        if target.total_pencapaian_angka_kredit != utama_penunjang:
            raise osv.except_osv(_('Invalid Action!'),
                                 _('Total Capaian Tidak sama dengan jumlah unsur utama dan penunjang'))
        min_persen = 90
        if target.is_guru:
            min_persen = 90
        else:
            min_persen = 80

            total_detail = 0
            if target.pak_detail_ids:
                total_detail = len(target.pak_detail_ids)
            if total_detail == 0:
                raise osv.except_osv(_('Invalid Action!'),
                                     _('Anda belum membuat perencanaan angka kredit.'))

            persen_utama = float(
                (target.total_pencapaian_angka_kredit_utama / target.total_pencapaian_angka_kredit)) * 100.00
            persen_penunjang = float(
                (target.total_pencapaian_angka_kredit_penunjang / target.total_pencapaian_angka_kredit)) * 100.00


            if persen_utama < min_persen:
                raise osv.except_osv(_('Invalid Action!'),
                                     _('Angka Kredit unsur utama tidak memenuhi batas minimum.'))

        return True





penentuan_angka_kredit()


class pak_name_category(osv.Model):
    _name = "pak.name.category"
    _description = "Kategori jenis kegiatan tahunan guru"

    _columns = {
        'name': fields.char('Kategori', size=50, required=True),
        'description': fields.text('Deskripsi'),
        'active': fields.boolean('Aktif'),
    }
    _defaults = {
        'active': 'True',
    }
pak_name_category()