from openerp.osv import osv
from openerp.tools.translate import _
from openerp import pooler
from openerp import SUPERUSER_ID

class penentuan_angka_kredit_confirm_massive(osv.osv):
    _name = "penentuan.angka.kredit.confirm.massive"
    _description = "Select All Confirm PAK"
    def action_confirm_all(self, cr, uid, ids, context=None):

        if context is None:
            context = {}
        pool_obj = pooler.get_pool(cr.dbname)
        pak_pool = pool_obj.get('penentuan.angka.kredit')

        if not ids :
            return {'type': 'ir.actions.act_window_close'}
        process_ids = []
        for obj in pak_pool.read(cr,uid,context['active_ids'],['id','state'], context) :
            if obj['state'] == 'evaluated' :
                    process_ids.append(obj['id'])
               
        
        if process_ids :
            pak_pool.action_confirm(cr, uid,process_ids, context=context)
        
        return {'type': 'ir.actions.act_window_close'}
    
penentuan_angka_kredit_confirm_massive()


class penentuan_angka_kredit_evaluated_pak_massive(osv.osv):
    _name = "penentuan.angka.kredit.evaluated.pak.massive"
    _description = "Select All Ajukan Verifikatur PAK"

    def action_pak_evaluated_all(self, cr, uid, ids, context=None):

        if context is None:
            context = {}
        pool_obj = pooler.get_pool(cr.dbname)
        pak_pool = pool_obj.get('penentuan.angka.kredit')

        if not ids:
            return {'type': 'ir.actions.act_window_close'}
        process_ids = []
        for obj in pak_pool.read(cr, uid, context['active_ids'], ['id', 'state'], context):
            if obj['state'] == 'propose':
                process_ids.append(obj['id'])

        if process_ids:
            pak_pool.action_pak_evaluated(cr, SUPERUSER_ID, process_ids, context=context)

        return {'type': 'ir.actions.act_window_close'}


penentuan_angka_kredit_evaluated_pak_massive()

class penentuan_angka_kredit_evaluated_massive(osv.osv):
    _name = "penentuan.angka.kredit.evaluated.massive"
    _description = "Select All Ajukan Verifikatur BKD"

    def action_evaluated_all(self, cr, uid, ids, context=None):

        if context is None:
            context = {}
        pool_obj = pooler.get_pool(cr.dbname)
        pak_pool = pool_obj.get('penentuan.angka.kredit')

        if not ids:
            return {'type': 'ir.actions.act_window_close'}
        process_ids = []
        for obj in pak_pool.read(cr, uid, context['active_ids'], ['id', 'state'], context):
            if obj['state'] == 'pak_evaluated':
                process_ids.append(obj['id'])

        if process_ids:
            pak_pool.action_evaluated(cr, SUPERUSER_ID, process_ids, context=context)

        return {'type': 'ir.actions.act_window_close'}


penentuan_angka_kredit_evaluated_massive()