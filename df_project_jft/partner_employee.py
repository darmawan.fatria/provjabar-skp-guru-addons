from openerp.osv import fields, osv

class res_partner(osv.Model):
    _inherit = 'res.partner'

    _columns = {
        'is_guru': fields.boolean('Guru', ),
    }
res_partner