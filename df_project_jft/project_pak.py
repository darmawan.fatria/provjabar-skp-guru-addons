from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
import time
from mx import DateTime
from openerp import netsvc

_MIN_UNSUR_UTAMA = 2
class project_pak(osv.Model):
    _name = "project.pak"
    _description ='Project PAK'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    def create(self, cr, uid, vals, context=None):
        if vals:
            for user_obj in self.pool.get('res.users').browse(cr, uid, uid, context=context):
                employee = user_obj.partner_id
                user_id_atasan = employee.user_id_atasan.user_id.id
                user_id_banding = employee.user_id_banding.user_id.id
                user_id_bkd = employee.company_id.user_id_bkd.id

                vals['user_id_atasan'] = user_id_atasan
                vals['user_id_banding'] = user_id_banding
                vals['user_id_bkd'] = user_id_bkd

        return super(project_pak, self).create(cr, uid, vals, context)



    _columns = {
        'pak_id': fields.many2one('penentuan.angka.kredit', 'Target DUPAK', ),

        'target_pak_type_id': fields.selection([('utama', 'Unsur Utama'),
                                                ('penunjang', 'Unsur Penunjang')
                                                ],
                                               'Jenis', required=False,
                                               ),
        'sub_target_pak_type_id': fields.selection([('pendidikan', 'Pendidikan'),
                                                    ('pembelajaran', 'Pembelajaran/Bimbingan Tugas Tertentu'),
                                                    ('pengembangan', 'Pengembangan Keprofesian Berkelanjutan'),
                                                    ('lain_lain', 'Lain Lain'),
                                                    ],
                                                   'Kategori', required=False,
                                                   ),
        'state': fields.selection([('draft', 'Draft'), ('new', 'Baru'),
                                   ('propose', 'Penilaian Atasan'), ('rejected_manager', 'Penilaian Ditolak'),
                                   ('evaluated', 'Verifikasi BKD'), ('rejected_bkd', 'Pengajuan Ditolak BKD'),
                                   ('confirm', 'Target Di Terima'),
                                   ('pending', 'Pending'),
                                   ('propose_to_close', 'Pengajuan Closing Target'), ('closed', 'Closed'),
                                   ('cancelled', 'Cancel'),
                                   ('propose_correction', 'Ajukan Perubahan Target'),
                                   ('correction', 'Revisi Target'),
                                   ], 'Status', required=False, ),
        'name': fields.char('Name', size=4, required=False),
        'target_period_year': fields.char('Periode', size=4, required=True),
        'target_angka_kredit': fields.float('Target AK', required=True
                                            , digits_compute=dp.get_precision('angka_kredit')),
        'target_angka_kredit_utama': fields.float('Unsur Utama', required=True
                                                , digits_compute=dp.get_precision('angka_kredit')),
        'target_angka_kredit_penunjang': fields.float('Unsur Penunjang', required=True
                                                      , digits_compute=dp.get_precision('angka_kredit')),
        'jumlah_kegiatan_utama': fields.integer('Jumlah Unsur Utama'),
        'jumlah_kegiatan_penunjang': fields.integer('Jumlah Unsur Penunjang'),
        'user_id': fields.many2one('res.users', 'Pegawai Yang Dinilai', ),
        'user_id_atasan': fields.many2one('res.users', 'Atasan Langsung', ),
        'user_id_banding': fields.many2one('res.users', 'Atasan Banding', ),
        'user_id_bkd': fields.many2one('res.users', 'Verifikatur', ),
        'user_id_pak': fields.many2one('res.users', 'Verifikatur PAK', ),
        'realisasi_lines': fields.one2many('project.project', 'project_pak_id', 'Target Tahunan', order='target_period_year'),
        'active': fields.boolean('Active'),
        'generate_dupak': fields.boolean('DUPAK Per Tahun'),
        'employee_id': fields.related('user_id', 'partner_id', relation='res.partner', type='many2one',
                                      string='Data Pegawai', store=True),
        'job_type': fields.related('employee_id', 'job_type', type='char', string='Tipe Jabatan', store=True),
        'is_guru': fields.related('employee_id', 'is_guru', type='boolean', string='Guru',
        store = True),
        'company_id': fields.many2one('res.company', 'OPD'),

        'count_correction': fields.integer('Jumlah Revisi', readonly=True),
        'count_evaluated': fields.integer('Jumlah Pengajuan Verifikasi', readonly=True),
    }
    _defaults = {
        'active': True,
        'target_period_year': lambda *args: time.strftime('%Y'),
        'user_id': lambda self, cr, uid, ctx: uid,
        'company_id': lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,
        'state': 'draft',
        'jumlah_kegiatan_utama' : _MIN_UNSUR_UTAMA,
        'jumlah_kegiatan_penunjang': 0
    }
    _order = "write_date desc"

    def generate_target_tahunan(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        task = {}

        task_pool = self.pool.get('project.project')
        #wf_service = netsvc.LocalService("workflow")
        #wf_service.trg_validate(uid, 'project.project', id, 'action_correction_approve', cr)

        for target_obj in self.browse(cr, uid, ids, context=context):
            task_ids = task_pool.search(cr, uid, [('project_pak_id', '=', target_obj.id)], context=None)
            if task_ids:
                for task_id in task_ids:
                    #wf_service.trg_validate(SUPERUSER_ID, 'project.project', task_id, 'act_cancel', cr)
                    #task_pool.write(cr, SUPERUSER_ID, task_id, {'active': False}, context=context)
                    task_pool.cancel_target(cr, SUPERUSER_ID, [task_id], context=context)
                task_pool.write(cr, SUPERUSER_ID, task_ids, {'active': False}, context=context)

            user_id = target_obj.user_id.id
            user_id_bkd = None
            user_id_pak = None
            employee = target_obj.user_id.partner_id
            if user_id != uid:
                raise osv.except_osv(_('Invalid Action!'),
                                     _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
            if not employee:
                raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                     _('Proses Tidak Dapat Dilanjutkan Karena Ada Beberapa Informasi Kepegawaian Belum Diisi, Khususnya Data Pejabat Penilai Dan Atasan Banding.'))
            else:
                company = employee.company_id
                company_id = company.id
                if not company_id:
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                         _('Proses Tidak Dapat Dilanjutkan Karena OPD/Dinas Pegawai Belum Dilengkapi.'))

                if not target_obj.user_id_bkd:
                    if not company.user_id_bkd:
                        raise osv.except_osv(_('Invalid Action, Data Dinas Kurang Lengkap'),
                                             _(
                                                 'Verifikatur Dari BKD Belum Diupdate,  Silahkan hubungi Admin Atau isi Data Pemeriksa.'))
                    else:
                        user_id_bkd = company.user_id_bkd.id
                else:
                    user_id_bkd = target_obj.user_id_bkd.id
                if not target_obj.user_id_pak:
                    if not company.user_id_pak:
                        raise osv.except_osv(_('Invalid Action, Data Dinas Kurang Lengkap'),
                                             _(
                                                 'Verifikatur PAK Belum Diupdate,  Silahkan hubungi Admin Atau isi Data Pemeriksa.'))
                    else:
                        user_id_pak = company.user_id_pak.id
                else:
                    user_id_pak = target_obj.user_id_pak.id

                if not employee.user_id_atasan:
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                         _(
                                             'Proses Tidak Dapat Dilanjutkan Karena Data Atasan Langsung (Pejabat Penilai) Belum Terisi.'))
                if not employee.user_id_atasan.user_id:
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                         _(
                                             'Proses Tidak Dapat Dilanjutkan Karena Data Atasan Langsung (Pejabat Penilai) Belum Melakukan Proses Aktifasi.'))
                if not employee.user_id_banding:
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                         _(
                                             'Proses Tidak Dapat Dilanjutkan Karena Data Atasan Banding (Pejabat Pengajuan Banding) Belum Terisi.'))

                if not employee.user_id_banding.user_id:
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                         _(
                                             'Proses Tidak Dapat Dilanjutkan Karena Data Atasan Banding (Pejabat Pengajuan Banding) Belum Melakukan Proses Aktifasi.'))

                if not employee.job_type or not employee.job_id or not employee.golongan_id:
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                         _(
                                             'Proses Tidak Dapat Dilanjutkan Karena Data Jabatan Dan Pangkat Belum Diisi.'))

                #if not target_obj.uraian:
                #    raise osv.except_osv(_('Invalid Action'),
                #                         _('Silahkan isi uraian kegiatan terlebih dahulu.'))

            user_id_atasan = target_obj.user_id_atasan.id
            user_id_banding = target_obj.user_id_banding.id

            if not target_obj.user_id_atasan.id:
                user_id_atasan = employee.user_id_atasan.user_id.id
            if not target_obj.user_id_banding.id:
                user_id_banding = employee.user_id_banding.user_id.id

            task.update({
                'project_pak_id': target_obj.id,
                'pak_id': target_obj.pak_id.id,
                'user_id': user_id,
                'company_id': company_id,
                'name': target_obj.name,
                #'code': target_code,
                'target_type_id' : 'sotk',
                'user_id_atasan': user_id_atasan or False,
                'user_id_banding': user_id_banding or False,
                'user_id_bkd': user_id_bkd or False,
                'user_id_pak': user_id_pak or False,
                'notes': '-',
                'active': True,
            })
            # Update Task Target Tahunan
            now = DateTime.today();

            #rincian unsur utama
            part_angka_kredit = 0
            sum_of_angka_kredit=0
            ang_kredit = target_obj.target_angka_kredit_utama
            lama_kegiatan = target_obj.jumlah_kegiatan_utama
            if ang_kredit > 0 and lama_kegiatan > 0:
                x_kredit = ang_kredit / lama_kegiatan
                y_kredit = ang_kredit % lama_kegiatan
                for i in range(0, lama_kegiatan):
                    # angka Kredit
                    if ang_kredit >0 :
                        part_angka_kredit = round(x_kredit)
                        sum_of_angka_kredit += part_angka_kredit
                    task.update({
                        'target_period_year': target_obj.target_period_year,
                        'target_jumlah_kuantitas_output': 1,
                        'target_waktu': 12,
                        'target_angka_kredit': part_angka_kredit,
                        'target_biaya': 0,
                        'target_satuan_kuantitas_output' : 1,
                        'target_satuan_waktu' : 'bulan',
                        'target_pak_type_id': 'utama',
                    })

                    task.update({'state': 'draft',})
                    if i == (lama_kegiatan - 1):
                        balancing_kredit = ang_kredit- sum_of_angka_kredit
                        task.update({
                            'target_angka_kredit': balancing_kredit + part_angka_kredit
                        })
                    # insert task
                    task_id = task_pool.create(cr, uid, task, context)

            # rincian unsur penunjang
            part_angka_kredit = 0
            sum_of_angka_kredit = 0
            ang_kredit = target_obj.target_angka_kredit_penunjang
            lama_kegiatan = target_obj.jumlah_kegiatan_penunjang
            if ang_kredit > 0 and lama_kegiatan > 0:
                x_kredit = ang_kredit / lama_kegiatan
                y_kredit = ang_kredit % lama_kegiatan

                for i in range(0, lama_kegiatan):
                    # angka Kredit
                    if ang_kredit > 0:
                        part_angka_kredit = round(x_kredit)
                        sum_of_angka_kredit += part_angka_kredit
                    task.update({
                        'target_period_year': target_obj.target_period_year,
                        'target_jumlah_kuantitas_output': 1,
                        'target_waktu': 12,
                        'target_angka_kredit': part_angka_kredit,
                        'target_biaya': 0,
                        'target_satuan_kuantitas_output': 1,
                        'target_satuan_waktu': 'bulan',
                        'target_pak_type_id': 'penunjang',
                    })

                    task.update({'state': 'draft',})
                    if i == (lama_kegiatan - 1):
                        balancing_kredit = ang_kredit - sum_of_angka_kredit
                        task.update({
                                'target_angka_kredit': balancing_kredit + part_angka_kredit
                         })
                    # insert task
                    task_id = task_pool.create(cr, uid, task, context)

        # Update Status Target Bulanan
           # update_target = {
           #     'status_target_bulanan': 'sudah',
           #     'user_id_atasan': user_id_atasan or False,
           #     'user_id_banding': user_id_banding or False,
           #     'user_id_bkd': user_id_bkd or False,
           # }

           #self.write(cr, uid, target_obj.id, update_target, context)

            return True;

    #workflow
    def get_auth_id(self, cr, uid, ids, type, context=None):
        if not isinstance(ids, list): ids = [ids]
        # print "CONTEXT : ", context
        by_pass_auth = False
        if context:
            by_pass_auth = context.get('bypass_auth', False)
        if by_pass_auth:
            return True;
        if uid == SUPERUSER_ID:
            return True;
        for target in self.browse(cr, uid, ids, context=context):

            if type == 'user_id':
                if target.user_id:
                    if target.user_id.id != uid:
                        raise osv.except_osv(_('Invalid Action!'),
                                             _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
            elif type == 'user_id_atasan':
                if target.user_id_atasan:
                    if target.user_id_atasan.id != uid:
                        raise osv.except_osv(_('Invalid Action!'),
                                             _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
            elif type == 'user_id_bkd':
                if target.user_id_bkd:
                    if target.user_id_bkd.id != uid:
                        raise osv.except_osv(_('Invalid Action!'),
                                             _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))

            else:
                return False;

        return True;

    def is_get_auth_id(self, cr, uid, ids, type, context=None):
        if not isinstance(ids, list): ids = [ids]
        # print "CONTEXT : ", context
        by_pass_auth = False
        if context:
            by_pass_auth = context.get('bypass_auth', False)
        if by_pass_auth:
            return True;
        if uid == 1:
            return True;
        for target in self.browse(cr, uid, ids, context=context):

            if type == 'user_id':
                if target.user_id:
                    if target.user_id.id != uid:
                        return False
            elif type == 'user_id_atasan':
                if target.user_id_atasan:
                    return False
            elif type == 'user_id_bkd':
                if target.user_id_bkd:
                    if target.user_id_bkd.id != uid:
                        return False

            else:
                return False;

        return True;

    #
    def is_user(self, cr, uid, ids, context=None):
        target_id = len(ids) and ids[0] or False
        if not target_id: return False
        return self.get_auth_id(cr, uid, [target_id], 'user_id', context=context)

    def is_manager(self, cr, uid, ids, context=None):

        target_id = len(ids) and ids[0] or False
        if not target_id: return False
        return self.get_auth_id(cr, uid, [target_id], 'user_id_atasan', context=context)

    def is_verificator(self, cr, uid, ids, context=None):
        target_id = len(ids) and ids[0] or False
        if not target_id: return False
        return self.get_auth_id(cr, uid, [target_id], 'user_id_bkd', context=context)

    def is_can_back_to_user(self, cr, uid, ids, context=None):
        for target_obj in self.browse(cr, uid, ids, context=context):
            if target_obj.state == 'propose':
                if target_obj.count_evaluated > 0:
                    raise osv.except_osv(_('Invalid Action!'),
                                         _('Kegiatan Ini Sudah Pernah Melalui Proses Verifikasi, Tidak Dapat Dikembalikan Ke Pegawai.'))
                else:
                    return True
            else:
                return True;
        return True;

    def set_draft(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'draft'}, context=context)

    def set_new(self, cr, uid, ids, context=None):
        #if self.is_can_back_to_user(cr, uid, ids, context):
        return self.write(cr, uid, ids, {'state': 'new','active':True}, context=context)

    def set_propose(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'propose'}, context=context)

    def set_propose_rejected(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'new'}, context=context)

    def set_evaluated(self, cr, uid, ids, context=None):
        self.counting_evaluated(cr, uid, ids, context)
        return self.write(cr, uid, ids, {'state': 'evaluated'}, context=context)

    def counting_evaluated(self, cr, uid, ids, context=None):
        for target_obj in self.browse(cr, uid, ids, context=context):
            x_count_evaluated = 1 + target_obj.count_evaluated
            self.write(cr, uid, [target_obj.id, ], {'count_evaluated': x_count_evaluated})

    def set_evaluate_rejected(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'propose'}, context=context)

    def set_confirm(self, cr, uid, ids, context=None):
        task_pool = self.pool.get('project.project')
        task_ids = task_pool.search(cr, uid,
                                    [('project_pak_id', 'in', ids), ('state', '=', 'draft')],
                                    context=None)
        #if task_ids:
            #task_pool.set_realisasi(cr, uid, task_ids, context=None)
            #task_pool.write(cr, uid, task_ids, {'active': True}, context=None)
        if task_ids:
            for task_id in task_ids :
                wf_service = netsvc.LocalService("workflow")
                wf_service.trg_validate(SUPERUSER_ID, 'project.project', task_id, 'act_new', cr)
        return self.write(cr, uid, ids, {'state': 'confirm'}, context=context)

    def cancel_target(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        task_pool = self.pool.get('project.project')
        wf_service = netsvc.LocalService("workflow")
        for target_id in ids:
            task_ids = task_pool.search(cr, uid, [('project_pak_id', '=', target_id)], context=None)
            if task_ids:
                for task_id in task_ids:
                    wf_service.trg_validate(SUPERUSER_ID, 'project.project', task_id, 'act_cancel', cr)
                    task_pool.write(cr, SUPERUSER_ID, task_id, {'active': False}, context=context)
            self.write(cr, SUPERUSER_ID, [target_id], {'state': 'cancelled'}, context=context)
        return True;

    def set_correction_reject(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'confirm'}, context=context)

    def counting_correction(self, cr, uid, ids, context=None):
        for target_obj in self.browse(cr, uid, ids, context=context):
            x_count_correction = target_obj.count_correction + 1
            self.write(cr, uid, [target_obj.id, ], {'count_correction': x_count_correction}, context=context)
        return True

    def approve_correction(self, cr, uid, ids, context=None):
        wf_service = netsvc.LocalService("workflow")
        self.counting_correction(cr, uid, ids, context)
        for id in ids:
            wf_service.trg_validate(uid, 'project.project', id, 'action_correction_approve', cr)

    def set_propose_correction(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'propose_correction'}, context=context)

    def set_propose_closing(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'propose_to_close'}, context=context)

    def set_cancelling_closed_target(self, cr, uid, ids, context=None):
        user_id_bkd = uid
        for target_obj in self.browse(cr, uid, ids, context=None):
            if target_obj.user_id_bkd:
                user_id_bkd = target_obj.user_id_bkd.id
                break
        return self.cancel_target(cr, user_id_bkd, ids, context=context)

    def set_closing_reject(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'confirm'}, context=context)

    def set_closing_approve(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        task_pool = self.pool.get('project.project')
        wf_service = netsvc.LocalService("workflow")
        for target_id in ids:
            task_ids = task_pool.search(cr, uid,
                                        [('project_pak_id', '=', target_id), ('state', 'in', ('draft', 'realisasi'))],
                                        context=None)
            if task_ids:
                for task_id in task_ids:
                    wf_service.trg_validate(SUPERUSER_ID, 'project.project', task_id, 'act_closing', cr)
                    task_pool.write(cr, SUPERUSER_ID, task_id, {'active': False}, context=context)
        self.write(cr, uid, ids, {'state': 'closed'}, context=context)
        return True
project_pak()