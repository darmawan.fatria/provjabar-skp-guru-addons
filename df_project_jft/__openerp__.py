##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2016
#
#
##############################################################################

{
    "name": "Target Dan Realisasi Untuk Penilaian Prestasi Kinerja GURU",
    "version": "6.0",
    "author": "Darmawan Fatriananda",
    "category": "SKP",
    "description": """
      SKP
    """,
    "website" : "http://-",
    "license" : "GPL-3",
    "depends": [
                "df_project",
                "df_skp_employee",
                ],
    'data': [
        'security/project_jft_security.xml',
        #'workflow/project_workflow.xml',
        'pak_view.xml',
        'project_pak_view.xml',
        'security/ir.model.access.csv',
        'project_pak_menu_view.xml',
        'partner_employee_view.xml',
        'company_view.xml',
        'wizard/pak_massive_process_view.xml'
        ],
    
    'installable': True,
    'active': True,

}
