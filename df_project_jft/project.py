from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp

class project(osv.Model):
    _inherit = "project.project"



    def onchange_pak_name_category(self, cr, uid, ids, pak_name_category_id, context=None):
        vals = {}

        if pak_name_category_id:
            cat = self.pool.get('pak.name.category').browse(cr,uid,pak_name_category_id);
            vals.update(
                    {'name': cat.name
            })

        return {'value': vals}
    _columns = {
    'pak_id': fields.many2one('penentuan.angka.kredit', 'Target DUPAK', ondelete='set null',),
    'project_pak_id': fields.many2one('project.pak', 'Rincian DUPAK', ondelete='set null', ),
    'target_pak_type_id': fields.selection([('utama', 'Unsur Utama'),
                                            ('penunjang', 'Unsur Penunjang')
                                         ],
                                         'Jenis Angka Kredit', required=False, readonly=True,
                                         states={'draft': [('readonly', False)], 'new': [('readonly', False)],
                                                       'correction': [('readonly', False)],
                                                       'confirm': [('readonly', False)]},
                                         ),
    'user_id_pak': fields.many2one('res.users', 'Verifikatur PAK', ),
    'pak_name_category_id': fields.many2one('pak.name.category', 'Name',help='Jenis kegiatan guru/jft yg didefinisikan' ),
    'code_opd': fields.char('Kode OPD', size=2, readonly=True,
                                states={'draft': [('readonly', False)], 'new': [('readonly', False)]}, ),
    'code_program': fields.char('Kode Kegiatan', size=3, readonly=True,
                                    states={'draft': [('readonly', False)], 'new': [('readonly', False)]}, ),
    'code_kegiatan': fields.char('Kode Kegiatan', size=3, readonly=True)
    }

project()

class skp_employee_yearly(osv.Model):
    _inherit = "skp.employee.yearly"
skp_employee_yearly()

